const express = require("express");
const cors = require("cors");

var corsOptions = {
    origin: "http://localhost:3000"
};

const app = express();

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/*const db = require("./app/models");
db.sequelize.sync();*/

// simple route
app.get("/", (req, res) => {
    res.status(200).send('Hello world!');
})

require("./app/routes/teacher.routes")(app);
require('./app/routes/discipline.routes')(app);
require('./app/routes/study_group.routes')(app);
require('./app/routes/student.routes')(app);
require('./app/routes/stream.routes')(app);
require('./app/routes/assessment.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});