-- получение всех дисциплин группы
create function get_disciplines_by_group_id(groupid in integer)
	returns table (discipline_id integer, discipline_name varchar(100))
as
$$
begin
	return query
		select discipline.discipline_id, discipline.discipline_name from discipline
		inner join stream on discipline.discipline_id = stream.discipline_id
		where group_id = groupid;
end;
$$ language plpgsql;

-- полная информация о потоке
create function get_streams_by_group_id(groupid in integer)
	returns table (stream_id integer, 
				   discipline_id integer, 
				   discipline_name varchar(100), 
				   teacher_id integer, 
				   teacher_name varchar(150), 
				   stream_year varchar(4), 
				   term varchar(1))
as
$$
begin
	return query
		select 
			Stream.stream_id,
			Stream.discipline_id, 
			Discipline.discipline_name,
			Stream.teacher_id, 
			Teacher.teacher_name,
			Stream.stream_year,
			Stream.term
			from Stream 
			inner join Teacher on Teacher.teacher_id = Stream.teacher_id
			inner join Discipline on Stream.discipline_id = Discipline.discipline_id
		where Stream.group_id = groupid;
end;
$$ language plpgsql;

-- Создание экземпляров оценки при создании записи учебного плана
create function create_grades_fun() 
	returns trigger as
$$
declare
	i integer;
begin 
	for i in (select student.student_id 
			  from student where student.group_id 
			  in (select stream.group_id from stream where stream.stream_id = 
				  (select classes.stream_id from classes where classes.class_id = new.class_id)))
	loop
		insert into grades (student_id, plan_id) values (i, new.plan_id);
	end loop;
	return new;
end;
$$ language plpgsql;

create trigger create_grades
	after insert
	on assignment
	for each row
execute procedure create_grades_fun();

-- создание посещаемости при создании занятия
create function create_attendance_fun() 
	returns trigger as
$$
declare
	i integer;
begin 
	for i in (select student.student_id 
			  from student where student.group_id 
			  in (select stream.group_id from stream where stream.stream_id = new.stream_id))
	loop
		insert into attendance (student_id, class_id) values (i, new.class_id);
	end loop;
	return new;
end;
$$ language plpgsql;

create trigger create_attendance
	after insert
	on classes
	for each row
execute procedure create_attendance_fun();


-- удаление экземпляров оценки при удалении записи учебного плана
create function delete_grades_fun() 
	returns trigger as
$$
begin 
	delete from grades where grades.plan_id = old.plan_id;
	return old;
end;
$$ language plpgsql;

create trigger delete_grades
	before delete
	on assignment
	for each row
execute procedure delete_grades_fun();

-- удаление посещаемости при удалении записи занятия
create function delete_attendance_fun() 
	returns trigger as
$$
begin 
	delete from attendance where attendance.class_id = old.class_id;
	return old;
end;
$$ language plpgsql;

create trigger delete_attendance
	before delete
	on classes
	for each row
execute procedure delete_attendance_fun();

-- получение все задания и спроки для потока
create function get_tasks_by_stream_id(streamid in integer)
	returns table (class_id integer, 
				   class_date date, 
				   task_id integer, 
				   task_type varchar(1), 
				   task_description varchar(150))
as
$$
begin
	return query
		select Assignment.plan_id, Assignment.class_id, Classes.class_date, Assignment.task_id, Task.task_type, Task.task_description from Assignment
		inner join Classes on Assignment.class_id = Classes.class_id
		inner join Task on Assignment.task_id = Task.task_id
		where Classes.stream_id = streamid;
end;
$$ language plpgsql;

--задания которые еще не закреплены за занятием
create function get_tasks(streamid in integer)
	returns table (task_id integer, 
				   task_description varchar(150))
as
$$
begin
	return query
		select Task.task_id, Task.task_description from Task 
		inner join Stream on Stream.discipline_id = Task.discipline_id
		where Task.task_id not in 
		(select Assignment.task_id from Assignment
		inner join Classes on Assignment.class_id = Classes.class_id
		inner join Task on Assignment.task_id = Task.task_id
		where Classes.stream_id = streamid) 
		and Stream.stream_id = streamid;
end;
$$ language plpgsql;

--
create function get_grades_by_plan_id(planid in integer)
	returns table (grade_id integer,
				   student_id integer,
				   student_name varchar(150),
				   grade varchar(1),
				   grade_date date)
as
$$
begin
	return query
		select grades.grade_id, student.student_id, 
		student.student_name, grades.grade, grades.grade_date from grades 
		inner join Assignment on Assignment.plan_id = Grades.plan_id
		inner join Student on Grades.student_id = Student.student_id
		where Grades.plan_id = planid;
end;
$$ language plpgsql;

--
create function get_attendance_by_class_id(classid in integer)
	returns table (att_id integer,
				   student_id integer,
				   student_name varchar(150),
				   presence varchar(1))
as
$$
begin
	return query
		select attendance.att_id, student.student_id, student.student_name, attendance.presence from attendance 
		inner join Classes on Attendance.class_id = Classes.class_id
		inner join Student on Attendance.student_id = Student.student_id
		where Attendance.class_id = classid;
end;
$$ language plpgsql;

--
create or replace function get_avg(stid in integer, strid in integer)
	returns float
as
$$
begin
	return avg(Grades.grade :: integer) from Grades
	inner join Assignment on Grades.plan_id = Assignment.plan_id
	inner join Classes on Assignment.class_id = Classes.class_id
	where Grades.student_id = stid and Classes.stream_id = strid;
end;
$$ language plpgsql;

--
create or replace function get_count_n(stid in integer, strid in integer)
	returns int
as
$$
begin
	return Count(Grades.grade) from Grades
	inner join Assignment on Grades.plan_id = Assignment.plan_id
	inner join Classes on Assignment.class_id = Classes.class_id
	where Grades.student_id = stid and Classes.stream_id = strid and Grades.grade = '2'	;
end;
$$ language plpgsql;
