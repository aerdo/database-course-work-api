-- Дисциплина
CREATE TABLE Discipline 
(
	discipline_id SERIAL PRIMARY KEY,
	discipline_name varchar(100)
);

-- Преподаватель
CREATE TABLE Teacher
(
	teacher_id SERIAL PRIMARY KEY,
	teacher_name varchar(150)
);

-- Группа
CREATE TABLE Study_Group
(
	group_id SERIAL PRIMARY KEY,
	group_name varchar(10)
);

-- Студент
CREATE TABLE Student
(
	student_id SERIAL PRIMARY KEY,
	student_name varchar(150),
	group_id integer
);

-- Поток
CREATE TABLE Stream
(
	stream_id SERIAL PRIMARY KEY,
	group_id integer,
	discipline_id integer,
	teacher_id integer,
	stream_year varchar(4), -- только 4 цифры
	term varchar(1) -- o-осень / s-весна
);

-- Задание
CREATE TABLE Task
(
	task_id SERIAL PRIMARY KEY,
	discipline_id integer,
	task_type varchar(1), -- l-лр / p-практ / t-тр / k-кп / c-кр
	task_description varchar(150)
);

-- Занятие
CREATE TABLE Classes
(
	class_id SERIAL PRIMARY KEY,
	stream_id integer,
	class_date date,
	class_type varchar(1) -- l-лр / p-практ / k-конс
);

-- Учебный план
CREATE TABLE Assignment
(
	plan_id SERIAL PRIMARY KEY,
	task_id integer,
	class_id integer -- занятие, на котором надо завершить выполнение задания
);

-- Успеваемость
CREATE TABLE Grades
(
	grade_id SERIAL PRIMARY KEY,
	student_id integer,
	plan_id integer,
	grade varchar(1), -- 0/2/3/4/5
	grade_date date
);

-- Посещаемость
CREATE TABLE Attendance
(
	att_id SERIAL PRIMARY KEY,
	student_id integer,
	class_id integer,
	presence varchar(1) -- 1-да / 0-нет
);

