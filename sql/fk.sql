alter table Stream add foreign key (group_id) 
references Study_Group(group_id) on update no action;

alter table Stream add foreign key (discipline_id) 
references Discipline(discipline_id) on update no action;

alter table Stream add foreign key (teacher_id) 
references Teacher(teacher_id) on update no action;

---------------------------------------------

alter table Student add foreign key (group_id) 
references Study_group(group_id) on update no action;

---------------------------------------------

alter table Task add foreign key (discipline_id) 
references Discipline(discipline_id) on update no action;

---------------------------------------------

alter table Classes add foreign key (stream_id) 
references Stream(stream_id) on update no action;

---------------------------------------------

alter table Assignment add foreign key (task_id) 
references Task(task_id) on update no action;

alter table Assignment add foreign key (class_id) 
references Classes(class_id) on update no action;

---------------------------------------------

alter table Grades add foreign key (student_id) 
references Student(student_id) on update no action;

alter table Grades add foreign key (plan_id) 
references Assignment(plan_id) on update no action;

---------------------------------------------

alter table Attendance add foreign key (student_id) 
references Student(student_id) on update no action;

alter table Attendance add foreign key (class_id) 
references Classes(class_id) on update no action;

