-- Stream stream_year varchar(4) только 4 цифры
alter table Stream add check (stream_year ~* '2[0-9][0-9][0-9]');

-- Stream term	varchar(1) Семестр (o/s)
alter table Stream add check (term in ('o', 's'));

-- Task task_type varchar(1) l-лр / p-практ / t-тр / k-кп / c-кр
alter table Task add check (task_type in ('l','p','t','k','c'));

-- Classes class_type varchar(1) l-лр / p-практ / k-конс
alter table Classes add check (class_type in ('l','p','k'));

-- Grades grade varchar(1) 0/2/3/4/5
alter table Grades add check (grade in ('0','2','3','4','5'));

-- Attendance presence varchar(1) 1-да / 0-нет
alter table Attendance add check (presence in ('0','1'));