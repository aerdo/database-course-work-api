module.exports = (sequelize, dataTypes) => {
    return  sequelize.define('task', {
            task_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            discipline_id: {
                type: dataTypes.INTEGER
            },
            task_type: {
                type: dataTypes.STRING(1)
            },
            task_description: {
                type: dataTypes.STRING(150)
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};