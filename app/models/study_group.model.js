module.exports = (sequelize, dataTypes) => {
    return sequelize.define('study_group', {
            group_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            group_name: {
                type: dataTypes.STRING(10)
            },
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};