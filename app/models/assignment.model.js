module.exports = (sequelize, dataTypes) => {
    return  sequelize.define('assignment', {
            plan_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            task_id: {
                type: dataTypes.INTEGER
            },
            class_id: {
                type: dataTypes.INTEGER
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};