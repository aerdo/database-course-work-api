module.exports = (sequelize, dataTypes) => {
    return sequelize.define('classes', {
            class_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            class_date: {
                type: dataTypes.DATE
            },
            class_type: {
                type: dataTypes.STRING(1)
            },
            stream_id: {
                type: dataTypes.INTEGER
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};