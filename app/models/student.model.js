module.exports = (sequelize, dataTypes) => {
    return sequelize.define('student', {
            student_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            student_name: {
                type: dataTypes.STRING(150)
            },
            group_id: {
                type: dataTypes.INTEGER
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};