module.exports = (sequelize, dataTypes) => {
    return  sequelize.define('stream', {
            stream_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            group_id: {
                type: dataTypes.INTEGER
            },
            discipline_id: {
                type: dataTypes.INTEGER
            },
            teacher_id: {
                type: dataTypes.INTEGER
            },
            stream_year: {
                type: dataTypes.STRING(4)
            },
            term: {
                type: dataTypes.STRING(1)
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};