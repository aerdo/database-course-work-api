const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const dataTypes = require("sequelize");

const sequelize = new Sequelize(dbConfig.database, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min
    }
});

try {
    sequelize.authenticate().then(()=> {
        console.log('Connection has been established successfully.')
    }).catch((err)=>{
        console.error('Unable to connect to the database:', err);
    })
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.assignments = require('./assignment.model')(sequelize, dataTypes);
db.attendance = require('./attendance.model')(sequelize, dataTypes);
db.classes = require('./classes.model')(sequelize, dataTypes);
db.disciplines = require('./discipline.model')(sequelize, dataTypes);
db.grades = require('./grades.model')(sequelize, dataTypes);
db.streams = require('./stream.model')(sequelize, dataTypes);
db.students = require('./student.model')(sequelize, dataTypes);
db.groups = require('./study_group.model')(sequelize, dataTypes);
db.tasks = require('./task.model')(sequelize, dataTypes);
db.teachers = require('./teacher.model.js')(sequelize, dataTypes);

module.exports = db;