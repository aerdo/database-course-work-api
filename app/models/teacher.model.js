module.exports = (sequelize, dataTypes) => {
    return sequelize.define('teacher', {
            teacher_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            teacher_name: {
                type: dataTypes.STRING(150)
            },
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};