module.exports = (sequelize, dataTypes) => {
    return sequelize.define('attendance', {
            att_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            student_id: {
                type: dataTypes.INTEGER
            },
            class_id: {
                type: dataTypes.INTEGER
            },
            presence: {
                type: dataTypes.STRING(1)
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};