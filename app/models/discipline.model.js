module.exports = (sequelize, dataTypes) => {
    return sequelize.define('discipline', {
            discipline_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            discipline_name: {
                type: dataTypes.STRING(100)
            },
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};