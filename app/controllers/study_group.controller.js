const db = require("../models");

const Group = db.groups;
const Attendance = db.attendance;
const Grades = db.grades;

exports.findAllGroups = (req, res) => {
    Group.findAll({order: [['group_name', 'ASC']]})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving teachers."
            });
        });
};

exports.findStreamsByGroupId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select * from get_streams_by_group_id(${id}) order by stream_year, term, stream_id, discipline_name`).then(data=>{
        res.send(data[0]);
    }).catch(err => {
        res.sendStatus(404)
    });

};

exports.deleteGroup = (req, res) => {
    const id = req.params.id;
    Group.destroy({
        where: { group_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Group was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Group."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Group: " + err
            });
        });
}

exports.createGroup = (req, res) => {
    const group = {
        group_name: req.body.group_name
    };

    Group.create(group)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Group"
            });
        });
}

//============================================
exports.findAttendanceByStreamId = (req, res) => {
    const id = req.params.id;
    Attendance.findAll({where: {stream_id: id}}).then((data)=>{
            res.send(data);
        }
    ).catch(()=>{
            res.sendStatus(404)
    })
}

exports.updateAttendance = (req, res) => {
    const id = req.params.id;
    Attendance.update(req.body, {
        where: { att_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Attendance was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Attendance."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Attendance: " + err
            });
        });
}

//===========================================
exports.findGradesByStreamId = (req, res) => {
    const id = req.params.id;
    Grades.findAll({where: {stream_id: id}}).then((data)=>{
            res.send(data);
        }
    ).catch(()=>
        {res.sendStatus(404)}
    )
}

exports.updateGrade = (req, res) => {
    const id = req.params.id;
    Grades.update(req.body, {
        where: { grade_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Grade was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Grade."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Grade: " + err
            });
        });
}


