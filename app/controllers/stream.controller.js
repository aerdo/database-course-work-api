const db = require("../models");
const Stream = db.streams;

const Class = db.classes;
const Assignment = db.assignments;

exports.findAllStreams = (req, res) => {
    Stream.findAll({order: [['stream_id', 'ASC'],['group_id', 'ASC']]})
        .then((data)=>{
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving streams."
            });
        });
}

exports.updateStream = (req, res) => {
    const id = req.params.id;
    Stream.update(req.body, {
        where: { stream_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Stream was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Stream."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Stream: " + err
            });
        });
};

// удаление
exports.deleteStream = (req, res) => {
    const id = req.params.id;
    Stream.destroy({
        where: { stream_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Stream was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Stream."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Stream: " + err
            });
        });
};

exports.createStream = (req, res) => {
    if (!req.body.stream_year) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const stream = {
        group_id: req.body.group_id,
        discipline_id: req.body.discipline_id,
        teacher_id: req.body.teacher_id,
        stream_year: req.body.stream_year,
        term: req.body.term
    };

    Stream.create(stream)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Stream"
            });
        });
};

//======================================================

exports.createClass = (req, res) => {
    const newClass = {
        class_date: req.body.class_date,
        class_type: req.body.class_type,
        stream_id: req.body.stream_id
    };

    Class.create(newClass)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Class"
            });
        });
}

exports.deleteClass = (req, res) => {
    const id = req.params.id;
    Class.destroy({
        where: { class_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Task was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Class."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Class: " + err
            });
        });
}

exports.findClassesByStreamId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select classes.class_id, classes.class_date, classes.class_type from classes where classes.stream_id = ${id} order by class_date`).then((data)=>{
            res.send(data[0]);
        }).catch(()=>{
            res.sendStatus(404)
        })
}

//======================================================

exports.createAssignment = (req, res) => {
    const newAssignment = {
        task_id: req.body.task_id,
        class_id: req.body.class_id
    };

    Assignment.create(newAssignment)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Assignment"
            });
        });
}

exports.deleteAssignment = (req, res) => {
    const id = req.params.id;
    Assignment.destroy({
        where: { plan_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Assignment was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Assignment."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Assignment: " + err
            });
        });
}

exports.findAssignmentsByStreamId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select * from get_tasks_by_stream_id(${id}) order by class_date`).then((data)=>{
            res.send(data[0]);
    }
    ).catch(()=>{
            res.sendStatus(404)
    })
}

exports.findSpareTasksByStreamId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select * from get_tasks(${id}) order by task_id`).then((data)=>{
            res.send(data[0]);
        }
    ).catch(()=>{
        res.sendStatus(404)
    })
}