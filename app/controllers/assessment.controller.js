const db = require("../models");

const Attendance = db.attendance;
const Grades = db.grades;

exports.findAttendanceByClassId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select * from get_attendance_by_class_id(${id}) order by student_name`).then((data)=>{
        res.send(data[0]);
    }).catch(()=>{
        res.sendStatus(404)
    })
}

exports.updateAttendance = (req, res) => {
    const id = req.params.id;
    Attendance.update(req.body, {
        where: { att_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Attendance was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Attendance."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Attendance: " + err
            });
        });
}

exports.findGradesByPlanId = (req, res) => {
    const id = req.params.id;
    db.sequelize.query(`select * from get_grades_by_plan_id(${id}) order by student_name`).then((data)=>{
        res.send(data[0]);
    }).catch(()=>{
        res.sendStatus(404)
    })
}

exports.updateGrades = (req, res) => {
    const id = req.params.id;
    Grades.update(req.body, {
        where: { grade_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Grade was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Grade."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Grade: " + err
            });
        });
}

exports.getAVG = (req, res) => {
    const id = req.params.id;
    const studId = req.params.studId;

    db.sequelize.query(`select get_avg(${studId},${id});`).then((data)=>{
        res.send(data[0][0]);
    }).catch(()=>{
        res.sendStatus(404)
    })
}

exports.getCount = (req, res) => {
    const id = req.params.id;
    const studId = req.params.studId;

    db.sequelize.query(`select get_count_n(${studId},${id});`).then((data)=>{
        res.send(data[0][0]);
    }).catch(()=>{
        res.sendStatus(404)
    })
}