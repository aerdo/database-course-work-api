const db = require("../models");

const Student = db.students;

exports.findAllStudents = (req, res) => {
    Student.findAll({order: [['group_id', 'ASC'],['student_name', 'ASC']]})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving students."
            });
        });
};

exports.findStudentsByGroup = (req, res) => {
    const id = req.params.id;
    Student.findAll({
        where: { group_id: id },
        order: [['student_name', 'ASC']],
    }).then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving students."
            });
        });
}

// обновление
exports.updateStudent = (req, res) => {
    const id = req.params.id;
    Student.update(req.body, {
        where: { student_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Student was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Student."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Student: " + err
            });
        });
};

// удаление
exports.deleteStudent = (req, res) => {
    const id = req.params.id;

    Student.destroy({
        where: { student_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Student was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Student."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Student: " + err
            });
        });
};

exports.createStudent = (req, res) => {
    if (!req.body.student_name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const student = {
        student_name: req.body.student_name
    };

    Student.create(student)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Student"
            });
        });
};

exports.getStudentsWOGroup = (req, res) => {
    db.sequelize.query(`select * from Student where Student.group_id is null`, {order: [['student_name', 'ASC']]})
        .then(data => {
            res.send(data[0]);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving students."
            });
        });
}