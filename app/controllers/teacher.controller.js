const db = require("../models");

const Teacher = db.teachers;

exports.createTeacher = (req, res) => {
    console.log(req.body.teacher_name);
    if (!req.body.teacher_name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const teacher = {
        teacher_name: req.body.teacher_name
    };

    Teacher.create(teacher)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Teacher"
            });
        });
};

// вывод всех записей
exports.findAllTeachers = (req, res) => {
    Teacher.findAll({order: [['teacher_name', 'ASC']]})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving teachers."
            });
        });
};

// обновление
exports.updateTeacher = (req, res) => {
    const id = req.params.id;
    Teacher.update(req.body, {
        where: { teacher_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Teacher was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot find Teacher."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Teacher: " + err
            });
        });
};

// удаление
exports.deleteTeacher = (req, res) => {
    const id = req.params.id;

    Teacher.destroy({
        where: { teacher_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    success: "Teacher was deleted successfully!"
                });
            } else {
                res.send({
                    message: "Cannot find Teacher."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error deleting Teacher: " + err
            });
        });
};