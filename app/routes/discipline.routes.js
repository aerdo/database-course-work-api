module.exports = app => {
    const disciplines = require("../controllers/discipline.controller");

    var router = require("express").Router();

    // дисциплины
    router.get("/disciplines", disciplines.findAllDisciplines);
    router.post("/disciplines", disciplines.createDiscipline);
    router.delete("/disciplines/:id", disciplines.deleteDiscipline);
    router.put("/disciplines/:id", disciplines.updateDiscipline);

    // Задания
    router.get("/disciplines/:id/tasks", disciplines.findTasksByDisciplineId);
    router.delete("/disciplines/tasks/:id", disciplines.deleteTask);
    router.post("/disciplines/tasks", disciplines.createTask);

    app.use('/api', router);
};