const assessments = require("../controllers/assessment.controller");

module.exports = app => {
    var router = require("express").Router();

    router.get("/grades/plan/:id", assessments.findGradesByPlanId);
    router.get("/asses/:studId/:id", assessments.getAVG);
    router.get("/asses/:studId/:id/count", assessments.getCount);
    router.get("/attendance/class/:id", assessments.findAttendanceByClassId);

    router.put("/grades/:id", assessments.updateGrades);
    router.put("/attendance/:id", assessments.updateAttendance)

    app.use('/api', router);
}