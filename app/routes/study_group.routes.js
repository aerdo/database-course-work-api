const groups = require('../controllers/study_group.controller');

module.exports = app => {
    var router = require("express").Router();

    router.get("/groups", groups.findAllGroups);
    router.get("/groups/:id/disciplines", groups.findStreamsByGroupId)
    router.delete("/groups/:id", groups.deleteGroup);
    router.post("/groups", groups.createGroup);

    router.get("/streams/:id/attendance", groups.findAttendanceByStreamId);
    router.get("/streams/:id/grades", groups.findGradesByStreamId);

    router.put("/attendance/:id",groups.updateAttendance);
    router.put("/grades/:id",groups.updateGrade);

    app.use('/api', router);
};